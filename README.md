# Handling Arrays

This is a little demostration of handling arrays with functions. The functions were tested with node 
inside the console.

Install node js and clone this repo for execute all examples contained within the file test.js.

Run `node test.js`

:rocket:

# Aditionally 

## (JavaScript) What differences are there between: `apply() call() bind()`
Although the syntax of the function `apply()` is almost identical to `call()`, the fundamental difference is that `call()` accepts a list of arguments, while `apply()` accepts a simple array with the arguments.

The `bind()` method create a new function, that when it's called, it assigns to its operator this the value delivered, 
with a sequence of arguments given preceding any when the function is called.

# [React/Redux Style Guide](https://gist.github.com/datchley/4e0d05c526d532d1b05bf9b48b174faf)

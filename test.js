var array1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
var array2 = [1, 2, 3, 4, 2, 5, 5, 5, 5, 6, 8, 8, 9];
var array3 = [1, 2, 3, 4, 5];

function exercise1(element) {
    var arrays = new Array();
    while (element.length > 0)
    arrays.push(element.splice(0, 3));
    return console.log(arrays);
}

function exercise2(element) {
    var arrays = new Array();
    for (let i = 0; i < element.length; i++){
        if (arrays.indexOf(element[i]) == -1) {
            arrays.push(element[i])
        }
    }
    return console.log(arrays);
}

function exercise3(element) {
    let array = element;
    var ultimo = element[element.length - 1];
    array.pop() && array.unshift(ultimo);
    return console.log(array);
}

function exercise4(element) {
    let array = element;
    var primero = element[0];
    array.shift() && array.push(primero);
    return console.log(array);
}
exercise1(array1);
exercise2(array2);
exercise3(array3);
exercise4(array3);